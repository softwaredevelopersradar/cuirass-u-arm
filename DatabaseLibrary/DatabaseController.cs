﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using ClientDataBase;
using KirasaUModelsDBLib;
using CuirassArmDataModel.Log;
using CuirassArmDataModel.Database;

namespace DatabaseLibrary
{
    public class DatabaseController : IDatabaseController
    {
        private readonly ClientDB _client;

        public bool IsConnected => _client.IsConnected();

        public event EventHandler OnConnectToDatabase;
        public event EventHandler OnDisconnectFromDatabase;
        public event EventHandler<string> OnDatabaseError;
        public event EventHandler<GlobalProperties> OnGlobalPropertiesUpdated;
        public event EventHandler<IEnumerable<TableFreqForbidden>> OnForbiddenFrequenciesUpdated;
        public event EventHandler<IEnumerable<TableFreqRangesRecon>> OnReconFrequenciesUpdated;
        public event EventHandler<IEnumerable<TableSectorsRecon>> OnSectorsUpdated;
        public event EventHandler<IEnumerable<TableRes>> OnFrsTableUpdated;

        public DatabaseController(string clientName = "Cuirass Arm", string databaseAddress = "127.0.0.1:8302") 
        {
            _client = new ClientDB(clientName, databaseAddress);
            SubscribeToUtilityEvents();
        }

        public void Connect() => _client.ConnectAsync();

        public void Disconnect() => _client.DisconnectAsync();

        private void SubscribeToTableEvents()
        {
            (_client.Tables[NameTable.TableRes] as ITableUpdate<TableRes>).OnUpTable += OnFrsUpdate;
            (_client.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += OnFreqRangesReconUpdate;
            (_client.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += OnFreqForbiddenUpdate;
            (_client.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable += OnSectorsReconUpdate;
        }

        private void OnSectorsReconUpdate(object sender, InheritorsEventArgs.TableEventArgs<TableSectorsRecon> eventArgs)
        {
            OnSectorsUpdated?.Invoke(this, eventArgs.Table);
        }

        private void OnFreqForbiddenUpdate(object sender, InheritorsEventArgs.TableEventArgs<TableFreqForbidden> eventArgs)
        {
            OnForbiddenFrequenciesUpdated?.Invoke(this, eventArgs.Table);
        }

        private void OnFreqRangesReconUpdate(object sender, InheritorsEventArgs.TableEventArgs<TableFreqRangesRecon> eventArgs)
        {
            OnReconFrequenciesUpdated?.Invoke(this, eventArgs.Table);
        }

        private void OnFrsUpdate(object sender, InheritorsEventArgs.TableEventArgs<TableRes> eventArgs)
        {
            OnFrsTableUpdated?.Invoke(this, eventArgs.Table);
        }

        private void UnsubscribeFromTableEvents()
        {
            (_client.Tables[NameTable.TableRes] as ITableUpdate<TableRes>).OnUpTable -= OnFrsUpdate;
            (_client.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable -= OnFreqRangesReconUpdate;
            (_client.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable -= OnFreqForbiddenUpdate;
            (_client.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable -= OnSectorsReconUpdate;
        }

        private void SubscribeToUtilityEvents() 
        {
            _client.OnConnect += _client_OnConnect;
            _client.OnDisconnect += _client_OnDisconnect;
            _client.OnErrorDataBase += _client_OnErrorDataBase;
        }

        private void _client_OnErrorDataBase(object sender, InheritorsEventArgs.OperationTableEventArgs e) =>
            OnDatabaseError?.Invoke(this, $"Error in table {e.TableName}, operation {e.Operation}, type error {e.TypeError}\r\n{e.GetMessage}");

        private void _client_OnDisconnect(object sender, InheritorsEventArgs.ClientEventArgs e)
        {
            OnDisconnectFromDatabase?.Invoke(this, EventArgs.Empty);
            UnsubscribeFromTableEvents();
        }

        private void _client_OnConnect(object sender, InheritorsEventArgs.ClientEventArgs e) 
        {
            OnConnectToDatabase?.Invoke(this, EventArgs.Empty);
            SubscribeToTableEvents();
        }

        public void Add(NameTable tableName, AbstractCommonTable data)
        {
            try
            {
                _client.Tables[tableName].Add(data);
            }
            catch (Exception e) { ArmLogger.Error(e); }
        }

        public void Delete(NameTable tableName, AbstractCommonTable data)
        {
            try
            {
                _client.Tables[tableName].Delete(data);
            }
            catch (Exception e) { ArmLogger.Error(e); }
        }

        public void Clear(NameTable tableName)
        {
            try
            {
                _client.Tables[tableName].Clear();
            }
            catch (Exception e) { ArmLogger.Error(e); }
        }

        public void Change(NameTable tableName, AbstractCommonTable data)
        {
            try
            {
                _client.Tables[tableName].Change(data);
            }
            catch (Exception e) { ArmLogger.Error(e); }
        }

        public async Task<List<TableFreqRanges>> LoadTableFreqForbidden() 
        {
            try 
            {
                return await _client.Tables[NameTable.TableFreqForbidden].LoadAsync<TableFreqRanges>();
            }
            catch (Exception e) 
            {
                ArmLogger.Error(e);
                return new List<TableFreqRanges>();
            }
        }

        public async Task<List<TableFreqRanges>> LoadTableFreqRangesRecon()
        {
            try
            {
                return await _client.Tables[NameTable.TableFreqRangesRecon].LoadAsync<TableFreqRanges>();
            }
            catch (Exception e)
            {
                ArmLogger.Error(e);
                return new List<TableFreqRanges>();
            }
        }

        public async Task<List<TableSectorsRecon>> LoadTableSectorsRecon()
        {
            try
            {
                return await _client.Tables[NameTable.TableSectorsRecon].LoadAsync<TableSectorsRecon>();
            }
            catch (Exception e)
            {
                ArmLogger.Error(e);
                return new List<TableSectorsRecon>();
            }
        }

        public async Task<List<GlobalProperties>> LoadGlobalProperties()
        {
            try
            {
                return await _client.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>();
            }
            catch (Exception e)
            {
                ArmLogger.Error(e);
                return new List<GlobalProperties>();
            }
        }
    }
}
