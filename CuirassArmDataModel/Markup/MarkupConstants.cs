﻿namespace CuirassArmDataModel.Markup
{
    public static class MarkupConstants
    {
        public const string MarkupFileName = "CustomMarkup.yaml";

        public const int SplitterSize = 2;

        public const int SettingsDefaultSize = 280;

        public const int MainTableMinWidth = 400;
        public const int SecondaryTableMinWidth = 300;
    }
}
