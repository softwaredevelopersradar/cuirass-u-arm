﻿namespace CuirassArmDataModel.Markup
{
    /// <summary>
    /// Class to store everything about window markup
    /// </summary>
    public class WindowMarkup
    {
        /*
         * Default values are set based on the markup of MainWindow.xaml
         */

        public bool AreSettingsVisible { get; set; } = true;

        public bool AreTablesVisible { get; set; } = true;
        public double TablesGridLength { get; set; } = 1;

        public bool AreGraphsVisible { get; set; } = true;
        public double GraphsGridLength { get; set; } = 2;

        public double TablesGridLeftSplitterPosition { get; set; } = 3;
        public double TablesGridRightSplitterPosition { get; set; } = 1;
        public double GraphsGridLeftSplitterPosition { get; set; } = 3;
        public double GraphsGridRightSplitterPosition { get; set; } = 1;
    }
}
