﻿namespace CuirassArmDataModel.Markup
{
    public interface IMarkupManager
    {
        WindowMarkup Markup { get; }

        bool Save(string filename = MarkupConstants.MarkupFileName);
        bool TryLoad(string filename = MarkupConstants.MarkupFileName);

        void ToDefaultMarkup();
    }
}
