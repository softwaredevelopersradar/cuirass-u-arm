﻿using System;
using System.IO;
using System.Text;
using YamlDotNet.Serialization;
using CuirassArmDataModel.Log;

namespace CuirassArmDataModel.Markup
{
    public class MarkupManager : IMarkupManager
    {
        public WindowMarkup Markup { get; private set; }

        public MarkupManager()
        {
            Markup = new WindowMarkup();
        }

        private static object LockObject = new object();


        public bool Save(string filename = MarkupConstants.MarkupFileName)
        {
            try
            {
                lock (LockObject)
                {
                    var serializer = new SerializerBuilder()
                        .Build();
                    using (var fs = File.Open(filename, FileMode.Create))
                    using (var sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        serializer.Serialize(sw, Markup);
                    }
                }
            }
            catch (Exception exception)
            {
                ArmLogger.Trace($"{exception}\r\n{exception.StackTrace}");
                return false;
            }
            return true;
        }

        /// <returns> returns true if load is successful </returns>
        public bool TryLoad(string filename = MarkupConstants.MarkupFileName)
        {
            try
            {
                LoadInner(filename);
            }
            catch (FileNotFoundException fileNotFoundException) 
            {
                //ignored
                return false;
            }
            catch (Exception exception)
            {
                ArmLogger.Trace($"{exception}\r\n{exception.StackTrace}");
                return false;
            }
            return true;
        }

        private void LoadInner(string filename)
        {
            lock (LockObject)
            {
                var deserializer = new DeserializerBuilder().Build();
                Markup = deserializer.Deserialize<WindowMarkup>(File.ReadAllText(filename));
            }
        }

        public void ToDefaultMarkup()
        {
            Markup = new WindowMarkup();
        }
    }
}
