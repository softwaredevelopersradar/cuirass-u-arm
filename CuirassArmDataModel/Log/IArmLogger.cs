﻿using System;

namespace CuirassArmDataModel.Log
{
    public interface IArmLogger
    {
        bool IsDatabaseLogEnabled { get; set; }
        bool IsServerLogEnabled { get; set; }
        bool IsTraceLogEnabled { get; set; }

        void Error(Exception e);
        void Error(Exception e, string message);
        void Error(string message);
        void Log(string message);
        void DatabaseLog(string message);
        void ServerLog(string message);
        void Trace(string message);
        void Warning(string message);
    }
}
