﻿using System;
using NLog;

namespace CuirassArmDataModel.Log
{
    public class NLogLogger : IArmLogger
    {
        public bool IsDatabaseLogEnabled { get; set; }
        public bool IsServerLogEnabled { get; set; }
        public bool IsTraceLogEnabled { get; set; }

        private readonly ILogger DatabaseLogger;
        private readonly ILogger ServerLogger;

        private readonly ILogger TraceLogger;
        private readonly ILogger ErrorLogger;

        public NLogLogger() 
        {
            try
            {
                DatabaseLogger = LogManager.GetLogger("Database");
                ServerLogger = LogManager.GetLogger("Server");

                ErrorLogger = LogManager.GetLogger("Error");
                TraceLogger = LogManager.GetLogger("Trace");

                var logConfig = LogManager.Configuration;
                if (logConfig == null)
                {
                    return;
                }
                if (logConfig.Variables.ContainsKey(nameof(IsDatabaseLogEnabled)))
                {
                    IsDatabaseLogEnabled = logConfig.Variables[nameof(IsDatabaseLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsServerLogEnabled)))
                {
                    IsServerLogEnabled = logConfig.Variables[nameof(IsServerLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsTraceLogEnabled)))
                {
                    IsTraceLogEnabled = logConfig.Variables[nameof(IsTraceLogEnabled)].Text == "true";
                }
            }
            // something get wrong - nlog config file doesn't exist or nlog assembly is not loaded
            catch
            {
                // ignored
                // in this case all logging is switched off
            }
        }

        public void DatabaseLog(string message)
        {
            if (IsDatabaseLogEnabled)
                Log(DatabaseLogger, message);
        }

        public void ServerLog(string message)
        {
            if (IsServerLogEnabled)
                Log(ServerLogger, message);
        }

        public void Error(Exception e) => Error(e, e.Message + "\n" + e.StackTrace);

        public void Error(Exception e, string message) => ErrorLogger.Error(e);

        public void Error(string message) => ErrorLogger.Error(message);

        public void Log(string message) => Log(TraceLogger, message);

        public void Trace(string message)
        {
            if (IsTraceLogEnabled)
                TraceLogger.Trace(message);
        }

        public void Warning(string message) => Log(TraceLogger, $"Warning!\r\n\r\n{message}");

        private void Log(ILogger logger, string message) => logger.Info(message);
    }
}
