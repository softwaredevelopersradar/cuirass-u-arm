﻿using System;

namespace CuirassArmDataModel.Log
{
    public static class ArmLogger
    {
        public static bool IsDatabaseLogEnabled
        {
            get => Logger.IsDatabaseLogEnabled;
            set => Logger.IsDatabaseLogEnabled = value;
        }

        public static bool IsServerLogEnabled
        {
            get => Logger.IsServerLogEnabled;
            set => Logger.IsServerLogEnabled = value;
        }

        public static bool IsTraceLogEnabled
        {
            get => Logger.IsTraceLogEnabled;
            set => Logger.IsTraceLogEnabled = value;
        }

        public static IArmLogger Logger { get; private set; }

        static ArmLogger()
        {
            Logger = new NullLogger();
        }

        public static void SetLogger(IArmLogger logger)
        {
            Logger = logger ?? throw new ArgumentException("logger can't be null!");
        }

        public static void Log(string message)
        {
            Logger.Log(message);
        }

        public static void ServerLog(string message)
        {
            Logger.ServerLog(message);
        }

        public static void DatabaseLog(string message)
        {
            Logger.DatabaseLog(message);
        }

        /// <summary>
        /// Traces inner app methods
        /// </summary>
        public static void Trace(string message)
        {
            Logger.Trace(message);
        }

        public static void Warning(string message)
        {
            Logger.Warning(message);
        }

        public static void Error(string message)
        {
            Logger.Error(message);
        }

        public static void Error(Exception e)
        {
            Logger.Error(e);
        }

        public static void Error(Exception e, string message)
        {
            Logger.Error(e, message);
        }
    }
}
