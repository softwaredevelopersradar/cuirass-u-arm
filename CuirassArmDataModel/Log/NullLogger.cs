﻿using System;

namespace CuirassArmDataModel.Log
{
    public class NullLogger : IArmLogger
    {
        public bool IsDatabaseLogEnabled { get; set; }
        public bool IsServerLogEnabled { get; set; }
        public bool IsTraceLogEnabled { get; set; }

        public void DatabaseLog(string message)
        {}

        public void Error(Exception e)
        {}

        public void Error(Exception e, string message)
        {}

        public void Error(string message)
        {}

        public void Log(string message)
        {}

        public void ServerLog(string message)
        {}

        public void Trace(string message)
        {}

        public void Warning(string message)
        {}
    }
}
