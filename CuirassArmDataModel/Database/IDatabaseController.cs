﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using KirasaUModelsDBLib;

namespace CuirassArmDataModel.Database
{
    public interface IDatabaseController
    {
        bool IsConnected { get; }

        event EventHandler OnConnectToDatabase;
        event EventHandler OnDisconnectFromDatabase;
        event EventHandler<string> OnDatabaseError;

        event EventHandler<GlobalProperties> OnGlobalPropertiesUpdated;
        event EventHandler<IEnumerable<TableFreqForbidden>> OnForbiddenFrequenciesUpdated;
        event EventHandler<IEnumerable<TableFreqRangesRecon>> OnReconFrequenciesUpdated;
        event EventHandler<IEnumerable<TableSectorsRecon>> OnSectorsUpdated;
        event EventHandler<IEnumerable<TableRes>> OnFrsTableUpdated;

        void Connect();
        void Disconnect();

        void Add(NameTable tableName, AbstractCommonTable data);
        void Delete(NameTable tableName, AbstractCommonTable data);
        void Clear(NameTable tableName);
        void Change(NameTable tableName, AbstractCommonTable data);

        Task<List<TableFreqRanges>> LoadTableFreqForbidden();
        Task<List<TableFreqRanges>> LoadTableFreqRangesRecon();
        Task<List<TableSectorsRecon>> LoadTableSectorsRecon();
        Task<List<GlobalProperties>> LoadGlobalProperties();
    }
}
