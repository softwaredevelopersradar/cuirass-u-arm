﻿using System;
using System.ComponentModel;
using System.Windows;
using ValuesCorrectLib;

namespace CuirassArm
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void SetLanguageTables(DllCuirassemProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllCuirassemProperties.Models.Languages.EN:
                        dict.Source = new Uri("/CuirassArm;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllCuirassemProperties.Models.Languages.RU:
                        dict.Source = new Uri("/CuirassArm;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/CuirassArm;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        private void PropertiesControl_OnLanguageChanged(object sender, DllCuirasaUProperties.Models.Languages language)
        {
            SetLanguageTables((DllCuirassemProperties.Models.Languages)language);
            TranslatorTables.LoadDictionary((DllCuirassemProperties.Models.Languages)language);

            var panoramaLanguage = language == DllCuirasaUProperties.Models.Languages.RU ? "rus" : "eng";
            PanoramaControl.SetLanguage(panoramaLanguage);
        }

        private void Common_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Language")
                return;

            PropertiesControl_OnLanguageChanged(sender, PropertiesControl.Local.Common.Language);
        }
    }
}
