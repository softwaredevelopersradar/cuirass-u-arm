﻿using System.ComponentModel;
using System.Windows;

namespace CuirassArm
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void UcFreqRangesRecon_OnIsWindowPropertyOpen(object sender, FreqRangesControl.FreqRangesProperty e)
        {
            var language = (DllCuirassemProperties.Models.Languages)PropertiesControl.Local.Common.Language;
            e.SetLanguagePropertyGrid(language);
        }

        private void UcFreqForbidden_OnIsWindowPropertyOpen(object sender, FreqRangesControl.FreqRangesProperty e)
        {
            var language = (DllCuirassemProperties.Models.Languages)PropertiesControl.Local.Common.Language;
            e.SetLanguagePropertyGrid(language);
        }

        private void UcSectorsRecon_OnIsWindowPropertyOpen(object sender, SectorsReconControl.SectorsReconProperty e)
        {
            var language = (DllCuirassemProperties.Models.Languages)PropertiesControl.Local.Common.Language;
            e.SetLanguagePropertyGrid(language);
        }
    }
}
