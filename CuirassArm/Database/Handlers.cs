﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using TableEvents;
using KirasaUModelsDBLib;

namespace CuirassArm
{
    public partial class MainWindow
    {
        void InitClientDB()
        {
            DatabaseController.OnConnectToDatabase += HandlerConnect;

            DatabaseController.OnFrsTableUpdated += OnUpTable_TableRES;
            DatabaseController.OnReconFrequenciesUpdated += OnUpTable_TableFreqRangesRecon;
            DatabaseController.OnForbiddenFrequenciesUpdated += OnUpTable_TableFreqForbidden;
            DatabaseController.OnSectorsUpdated += OnUpTable_TableSectorsRecon;
        }

        private void HandlerConnect(object sender, EventArgs e) => LoadTables();

        private void OnUpTable_TableRES(object sender, IEnumerable<TableRes> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lRES = e.ToList();
                if (lRES.Count == 0) { ucRES.UpdateRES(lRES); }
                else { ucRES.AddRES(lRES); }
            });
        }

        private void UcRES_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            if (e.Id <= 0)
            {
                return;
            }
            //todo : add selected row frequency to event args, and send it to nb frequency set control
        }

        private void OnUpTable_TableFreqRangesRecon(object sender, IEnumerable<TableFreqRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqRangesRecon = (from t in e let a = t as TableFreqRanges select a).ToList();
                ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);
            });

            var list = e.Where(r => r.IsActive).ToList();

            var MinFreqs = new double[list.Count];
            var MaxFreqs = new double[list.Count];

            for (int i = 0; i < list.Count; i++)
            {
                MinFreqs[i] = list[i].FreqMinKHz / 1000d;
                MaxFreqs[i] = list[i].FreqMaxKHz / 1000d;
            }
            Dispatcher?.Invoke(()=> 
            {
                PanoramaControl.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, MinFreqs, MaxFreqs);
            });
        }

        private void OnUpTable_TableFreqForbidden(object sender, IEnumerable<TableFreqForbidden> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqForbidden = (from t in e let a = t as TableFreqRanges select a).ToList();
                ucFreqForbidden.UpdateFreqRanges(lFreqForbidden);
            });
        }

        private void OnUpTable_TableSectorsRecon(object sender, IEnumerable<TableSectorsRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSectorsRecon = (from t in e let a = t as TableSectorsRecon select a).ToList();
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);
            });
        }

        private async void LoadTables()
        {
            //todo : resolve
            var globalProperties = await DatabaseController.LoadGlobalProperties();
            if (globalProperties.Count != 0)
            {
                var mappedProperties = PropertiesHandling.GlobalPropertiesMapper.Map(globalProperties[0]);
                PropertiesControl.Global = mappedProperties;
            }

            lFreqForbidden = await DatabaseController.LoadTableFreqForbidden();
            ucFreqForbidden.UpdateFreqRanges(lFreqForbidden);

            lFreqRangesRecon = await DatabaseController.LoadTableFreqRangesRecon();
            ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);

            lSectorsRecon = await DatabaseController.LoadTableSectorsRecon();
            ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);

            var list = lFreqRangesRecon.Where(r => r.IsActive).ToList();

            var MinFreqs = new double[list.Count];
            var MaxFreqs = new double[list.Count];

            for (int i = 0; i < list.Count; i++)
            {
                MinFreqs[i] = list[i].FreqMinKHz / 1000d;
                MaxFreqs[i] = list[i].FreqMaxKHz / 1000d;
            }
            Dispatcher?.Invoke(() =>
            {
                PanoramaControl.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, MinFreqs, MaxFreqs);
            });
        }
    }
}
