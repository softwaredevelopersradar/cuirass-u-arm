﻿using System;
using CuirassArmDataModel.Log;

namespace CuirassArm
{
    public partial class MainWindow
    {
        private void SubscribeToDatabaseEvents() 
        {
            DatabaseController.OnConnectToDatabase += DatabaseController_OnConnectToDatabase;
            DatabaseController.OnDisconnectFromDatabase += DatabaseController_OnDisconnectFromDatabase;
            DatabaseController.OnDatabaseError += DatabaseController_OnDatabaseError;
        }

        private void DatabaseController_OnDatabaseError(object sender, string e)
        {
            ArmLogger.Error(e);
        }

        private void DatabaseController_OnDisconnectFromDatabase(object sender, EventArgs e)
        {
            Dispatcher?.Invoke(() => DatabaseConnectionControl.ShowDisconnect());            
        }

        private void DatabaseController_OnConnectToDatabase(object sender, EventArgs e)
        {
            Dispatcher?.Invoke(()=> DatabaseConnectionControl.ShowConnect());
        }

        private void ConnectToDatabase() => DatabaseController.Connect();
        private void DisconnectFromDatabase() => DatabaseController.Disconnect();
    }
}
