﻿using KirasaUModelsDBLib;
using System;
using TableEvents;

namespace CuirassArm
{
    public partial class MainWindow
    {
        public void InitTables()
        {
            // Таблица ИРИ
            ucRES.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteFrsRecord);
            ucRES.OnClearRecords += new EventHandler<NameTable>(OnClearFrsRecords);
            ucRES.OnSelectedRow += new EventHandler<SelectedRowEvents>(UcRES_OnSelectedRow);

            // Таблица Запрещенные частоты (ИЧ)
            ucFreqForbidden.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqForbidden.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqForbidden.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqForbidden.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqForbidden.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>( UcFreqForbidden_OnIsWindowPropertyOpen);

            // Таблица Диапазоны радиоразведки (ДРР)
            ucFreqRangesRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqRangesRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqRangesRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqRangesRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqRangesRecon.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>(UcFreqRangesRecon_OnIsWindowPropertyOpen);

            // Таблица Сектора радиоразведки (СРР)
            ucSectorsRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSectorsRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSectorsRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucSectorsRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSectorsRecon.OnIsWindowPropertyOpen += new EventHandler<SectorsReconControl.SectorsReconProperty>(UcSectorsRecon_OnIsWindowPropertyOpen);

        }
    }
}
