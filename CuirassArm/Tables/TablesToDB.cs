﻿using KirasaUModelsDBLib;
using System.Collections.Generic;
using TableEvents;

namespace CuirassArm
{
    public partial class MainWindow
    {
        // ИРИ
        public List<TableRes> lRES = new List<TableRes>();
        // Известные частоты (ИЧ)
        public List<TableFreqRanges> lFreqForbidden = new List<TableFreqRanges>();
        // Диапазоны радиоразведки (ДРР)
        public List<TableFreqRanges> lFreqRangesRecon = new List<TableFreqRanges>();
        // Сектора радиоразведки (СРР)
        public List<TableSectorsRecon> lSectorsRecon = new List<TableSectorsRecon>();



        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            DatabaseController.Add(e.NameTable, e.Record);
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            DatabaseController.Clear(nameTable);
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            DatabaseController.Delete(e.NameTable, e.Record);
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            DatabaseController.Change(e.NameTable, e.Record);
        }

        // Удалить все записи
        private void OnClearFrsRecords(object sender, NameTable nameTable)
        {
            DatabaseController.Clear(nameTable);
            _dspClient.ClearFrsSignalStorage();
        }

        // Удалить запись
        private void OnDeleteFrsRecord(object sender, TableEvent e)
        {
            DatabaseController.Delete(e.NameTable, e.Record);
            _dspClient.RemoveFrsSignal(e.Record.Id);
        }
    }
}
