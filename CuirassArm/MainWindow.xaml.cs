﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using CuirassArmDataModel.Database;
using CuirassArmDataModel.Log;
using CuirassArmDataModel.Markup;
using DatabaseLibrary;
using DspClientLibrary;
using CuirassGraphsLibrary;
using CuirassArm.PropertiesHandling;

namespace CuirassArm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private readonly IMarkupManager MarkupManager;
        private Markup.SettingsColumnState _settingsColumnState;
        private WindowMarkup Markup => MarkupManager.Markup;

        private readonly IDatabaseController DatabaseController;
        private readonly DspClient _dspClient;

        private const int SpectrumDelay = 20;

        private bool IsVideoSpectrumWorking = false;
        private bool IsNarrowBandSpectrumWorking = false;

        public MainWindow()
        {
            InitializeComponent();
            ArmLogger.SetLogger(new NLogLogger());

            PropertiesEventHandler.InitializePropertiesControl(PropertiesControl);
            DatabaseController = CreateDatabaseController();
            PropertiesEventHandler.InitializeDatabaseController(DatabaseController);
            var dspSettings = PropertiesControl.Local.DF;
            _dspClient = new DspClient(dspSettings.IpAddress, dspSettings.Port);
            _dspClient.OnConnect += _dspClient_OnConnect;
            _dspClient.OnDisconnect += _dspClient_OnDisconnect;
            _dspClient.AntennasStateReceived += _dspClient_AntennasStateReceived;

            SubscribeToDatabaseEvents();

            MarkupManager = new MarkupManager();
            var loadResult = MarkupManager.TryLoad();
            if (loadResult)
                ApplyMarkup();
            
            InitTables();
            PropertiesControl_OnLanguageChanged(PropertiesControl, PropertiesControl.Local.Common.Language);
            PropertiesControl.OnLanguageChanged += PropertiesControl_OnLanguageChanged;
            PropertiesControl.Local.Common.PropertyChanged += Common_PropertyChanged;
            PropertiesControl.OnGlobalPropertiesChanged += PropertiesControl_OnGlobalPropertiesChanged;
            PropertiesControl.OnPasswordChecked += PropertiesControl_OnPasswordChecked;
            PanoramaControl.GlobalNumberOfBands = 36;
            PanoramaControl.GlobalBandWidthMHz = 500;
            PanoramaControl.GlobalRangeXmin = 100;
            PanoramaControl.DotsPerBand = 466;

            //todo: to method
            //graphs initialization
            NarrowBandGraph.InitializeChart(2,0, 0, 512, 20, 120, 512);
            NarrowBandGraph.MeasureModeChanged += NarrowBandGraph_MeasureModeChanged;

            ucRES.OnMetering += UcRES_OnMetering;

            NarrowBandModel = NarrowBandGraph.DataContext as GraphViewModel;

            PanoramaControl.ThresholdChange += PanoramaControl_ThresholdChange;
            VideoGraph.VideoModeChanged += VideoGraph_OnVideoWorkingBandChanged;
            VideoGraph.AttackThresholdChanged += VideoGraph_AttackThresholdChanged;
            VideoGraph.PicThresholdChanged += VideoGraph_PicThresholdChanged;

            MasterWideAttackSlider.OnValueUpdated += MasterWideAttackSlider_OnValueUpdated;
            MasterWidePicSlider.OnValueUpdated += MasterWidePicSlider_OnValueUpdated;
            MasterNarrowAttackSlider.OnValueUpdated += MasterNarrowAttackSlider_OnValueUpdated;
            MasterNarrowPicSlider.OnValueUpdated += MasterNarrowPicSlider_OnValueUpdated;
            SlaveWideAttackSlider.OnValueUpdated += SlaveWideAttackSlider_OnValueUpdated;
            SlaveWidePicSlider.OnValueUpdated += SlaveWidePicSlider_OnValueUpdated;
            SlaveNarrowAttackSlider.OnValueUpdated += SlaveNarrowAttackSlider_OnValueUpdated;
            SlaveNarrowPicSlider.OnValueUpdated += SlaveNarrowPicSlider_OnValueUpdated;

            PanoramaControl.PreviewMouseUp += PanoramaControl_PreviewMouseUp;
            PanoramaControl.MouseDoubleClick += PanoramaControl_MouseDoubleClick;

            PanoramaControl.iControl.GlobalRangeYmax = 1000;

            AntennaFrequencySetting.SettingsChanged += NarrowFrequencyAntennaSetting_SettingsChanged;

            SubscribeToAdditionalSettingsChanges();

            TryToConnectToDspServer();
        }

        private void NarrowBandGraph_MeasureModeChanged(object sender, bool e)
        {
            _dspClient.SetNarrowMeasureStrategy(e);
        }

        private void PropertiesControl_OnPasswordChecked(object sender, bool e)
        {
            SeriesCalculationButton.Visibility = Visibility.Visible;
            CoefficientToggle.Visibility = Visibility.Visible;
            IqGatheringButton.Visibility = Visibility.Visible;
        }

        private void PropertiesControl_OnGlobalPropertiesChanged(object sender, DllCuirasaUProperties.Models.GlobalProperties e)
        {
            var globalProperties = GlobalPropertiesMapper.Map(PropertiesControl.Global);
            globalProperties.Id = 1; // wow, right?
            DatabaseController.Change(KirasaUModelsDBLib.NameTable.GlobalProperties, globalProperties); 
        }

        private void SlaveNarrowPicSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(false, false, false, (int)e);
        }

        private void SlaveNarrowAttackSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(false, false, true, (int)e);
        }

        private void SlaveWidePicSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(false, true, false, (int)e);
        }

        private void SlaveWideAttackSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(false, true, true, (int)e);
        }

        private void MasterNarrowPicSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(true, false, false, (int)e);
        }

        private void MasterNarrowAttackSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(true, false, true, (int)e);
        }

        private void MasterWidePicSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(true, true, false, (int)e);
        }

        private void MasterWideAttackSlider_OnValueUpdated(object sender, double e)
        {
            VideoGraph.SetThreshold(true, true, true, (int)e);
        }

        private void UcRES_OnMetering(object sender, TableEvents.TableEvent e)
        {
            try 
            {
                var record = e.Record as KirasaUModelsDBLib.TableRes;
                _dspClient.SetSignalForMeasuring(record.Id);
            }
            catch 
            {}
        }

        private void PanoramaControl_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!isMeasureBandExist)
            {
                return;
            }

            var bandRange = PanoramaControl.spControl.GetBandRange();
            measureBandStart = bandRange.Start;
            measureBandEnd = bandRange.End;
            SetMeasurementband();
        }

        private void PanoramaControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (isMeasureBandExist)
            {
                measureBandStart = 0;
                measureBandEnd = 0;
                SetMeasurementband();
            }
            isMeasureBandExist = !isMeasureBandExist;
        }

        private bool isMeasureBandExist = false;
        private double measureBandStart = 0;
        private double measureBandEnd = 0;

        private void SetMeasurementband()
        {
            _dspClient.SetWideMeasureBand((float)measureBandStart, (float)measureBandEnd);
        }

        private void NarrowFrequencyAntennaSetting_SettingsChanged(object sender, FrequencyAntennaSetup setup)
        {
            _dspClient.SetAntennaFrequencySettings(setup.FrequencyMhz, setup.Antennas);
        }

        private void VideoGraph_AttackThresholdChanged(object sender, int threshold)
        {
            var model = VideoGraph.Model;
            var isMaster = model.Device == FtmDevice.Master;
            var isWide = model.Mode == BandMode.Wideband;
            _dspClient.SetAttackThreshold(threshold, isMaster, isWide);

            SliderControl thresholdSlider;
            if (isMaster)
                thresholdSlider = isWide ? MasterWideAttackSlider : MasterNarrowAttackSlider;
            else
                thresholdSlider = isWide ? SlaveWideAttackSlider : SlaveNarrowAttackSlider;
            thresholdSlider.Value = threshold;
        }

        private void VideoGraph_PicThresholdChanged(object sender, int threshold)
        {
            var model = VideoGraph.Model;
            var isMaster = model.Device == FtmDevice.Master;
            var isWide = model.Mode == BandMode.Wideband;
            _dspClient.SetPicThreshold(threshold, isMaster, isWide);

            SliderControl thresholdSlider;
            if (isMaster)
                thresholdSlider = isWide ? MasterWidePicSlider : MasterNarrowPicSlider;
            else
                thresholdSlider = isWide ? SlaveWidePicSlider : SlaveNarrowPicSlider;
            thresholdSlider.Value = threshold;
        }

        private DatabaseController CreateDatabaseController() 
        {
            var name = $"Cuirass U Arm {PropertiesControl.Local.Common.AWS}";
            var connectionString = $"{PropertiesControl.Local.DB.IpAddress}:{PropertiesControl.Local.DB.Port}";
            return new DatabaseController(name, connectionString);
        }

        private void TryToConnectToDspServer() 
        {
            _dspClient.Connect();
        }

        private void LoadAdditionalSettings() 
        {
            //todo : split in methods
            var pairs = _dspClient.GetPairsMode();
            PairsModeToggleButton.IsChecked = pairs == DspDataModel.RdfPairsMode.FastPairs;

            var window = _dspClient.GetWindowType();
            WindowTypeToggleButton.IsChecked = window == DspDataModel.Hardware.WindowFunctionType.RectangleWindow;

            var strobe = _dspClient.GetStrobeMode();
            StrobeModeToggleButton.IsChecked = strobe == DspDataModel.Hardware.StrobeMode.Auto;

            var second = _dspClient.GetSecondsMark();
            SecondsMarkToggleButton.IsChecked = second == DspDataModel.Hardware.SecondsMark.ExternalMark;

            GetVideoThresholds();

            var masterDelay = _dspClient.GetFftDelay(DspDataModel.FtmRole.Master, DspDataModel.Hardware.VideoWorkingBand.Wide);
            masterDelaySlider.Value = masterDelay;

            var slaveDelay = _dspClient.GetFftDelay(DspDataModel.FtmRole.Slave, DspDataModel.Hardware.VideoWorkingBand.Wide);
            slaveDelaySlider.Value = slaveDelay;

            var masterNarrowDelay = _dspClient.GetFftDelay(DspDataModel.FtmRole.Master, DspDataModel.Hardware.VideoWorkingBand.Narrow);
            masterNarrowDelaySlider.Value = masterNarrowDelay;

            var slaveNarrowDelay = _dspClient.GetFftDelay(DspDataModel.FtmRole.Slave, DspDataModel.Hardware.VideoWorkingBand.Narrow);
            slaveNarrowDelaySlider.Value = slaveNarrowDelay;

            var masterAttenuator = _dspClient.GetCommutatorAttenuator(DspDataModel.FtmRole.Master);
            masterAttenuatorSlider.Value = masterAttenuator;

            var slaveAttenuator = _dspClient.GetCommutatorAttenuator(DspDataModel.FtmRole.Slave);
            slaveAttenuatorSlider.Value = slaveAttenuator;

            var isNarrowMeasuringAllowed = _dspClient.GetNarrowMeasureStrategy();
            NarrowBandGraph.SetMeasureMode(isNarrowMeasuringAllowed);

            var isWaterfallEnabled = _dspClient.GetWaterfallStrategy();
            WaterfallToggleButton.IsChecked = isWaterfallEnabled;

            var frequencyCoefficientRanges = _dspClient.GetFrequencyCoefficientRanges();
            CoefficientsGrid.Children.Clear();
            foreach (var range in frequencyCoefficientRanges)
            {
                var slider = new SliderControl()
                {
                    Title = $"{range.StartFrequencyMhz} - {range.EndFrequencyMhz} MHz",
                    Minimum = 1,
                    Maximum = 100,
                    LargeChange = 0.1,
                    SliderType = SliderControlValueType.DoubleSlider,
                    Value = range.Coefficient
                };
                slider.OnValueUpdated += (s,a) => _dspClient.SetFrequencyCoefficientRange(range.StartFrequencyMhz, range.EndFrequencyMhz, (float)a);
                CoefficientsGrid.Children.Add(slider);
            }
        }

        private void SubscribeToAdditionalSettingsChanges() 
        {
            masterDelaySlider.OnValueUpdated += MasterDelaySlider_OnValueUpdated;
            slaveDelaySlider.OnValueUpdated += SlaveDelaySlider_OnValueUpdated;
            masterNarrowDelaySlider.OnValueUpdated += MasterNarrowDelaySlider_OnValueUpdated;
            slaveNarrowDelaySlider.OnValueUpdated += SlaveNarrowDelaySlider_OnValueUpdated;
            masterAttenuatorSlider.OnValueUpdated += MasterAttenuatorSlider_OnValueUpdated;
            slaveAttenuatorSlider.OnValueUpdated += SlaveAttenuatorSlider_OnValueUpdated;
        }

        private void SlaveAttenuatorSlider_OnValueUpdated(object sender, double attenuator)
        {
            _dspClient.SetCommutatorAttenuator(DspDataModel.FtmRole.Slave, (int)attenuator);
        }

        private void MasterAttenuatorSlider_OnValueUpdated(object sender, double attenuator)
        {
            _dspClient.SetCommutatorAttenuator(DspDataModel.FtmRole.Master, (int)attenuator);
        }

        private void SlaveNarrowDelaySlider_OnValueUpdated(object sender, double delay)
        {
            _dspClient.SetFftDelay((int)delay, DspDataModel.FtmRole.Slave, DspDataModel.Hardware.VideoWorkingBand.Narrow);
        }

        private void MasterNarrowDelaySlider_OnValueUpdated(object sender, double delay)
        {
            _dspClient.SetFftDelay((int)delay, DspDataModel.FtmRole.Master, DspDataModel.Hardware.VideoWorkingBand.Narrow);
        }

        private void SlaveDelaySlider_OnValueUpdated(object sender, double delay)
        {
            _dspClient.SetFftDelay((int)delay, DspDataModel.FtmRole.Slave, DspDataModel.Hardware.VideoWorkingBand.Wide);
        }

        private void MasterDelaySlider_OnValueUpdated(object sender, double delay)
        {
            _dspClient.SetFftDelay((int)delay, DspDataModel.FtmRole.Master, DspDataModel.Hardware.VideoWorkingBand.Wide);
        }

        private void VideoGraph_OnVideoWorkingBandChanged(object sender, BandMode bandMode)
        {
            var workingBand = (DspDataModel.Hardware.VideoWorkingBand) bandMode;
            _dspClient.SetVideoWorkingBand(workingBand);
        }

        private void PanoramaControl_ThresholdChange(object sender, int value)
        {
            _dspClient.SetRdfThreshold(value + 130);//todo : to constants
        }

        private void _dspClient_AntennasStateReceived(object sender, DspDataModel.Server.AntennasState e)
        {
            Dispatcher?.Invoke(()=> 
            {
                switch (e.SwitchSource)
                {
                    case DspDataModel.Hardware.AntennaSwitchSource.RdfSpectrumTask:
                        AntennaFrequencySetting.TurnOn(e.WorkingAntennas);
                        break;
                    case DspDataModel.Hardware.AntennaSwitchSource.VideoSpectrumTask:
                        AntennaFrequencySetting.TurnOn(e.WorkingAntennas);
                        break;
                    case DspDataModel.Hardware.AntennaSwitchSource.NarrowSpectrumTask:
                        AntennaFrequencySetting.TurnOn(e.WorkingAntennas);
                        break;
                }
            });
        }

        private GraphViewModel NarrowBandModel;

        private void _dspClient_OnDisconnect(object sender, EventArgs e)
        {
            DspConnectionControl.ShowDisconnect();
        }

        private void _dspClient_OnConnect(object sender, EventArgs e)
        {
            DspConnectionControl.ShowConnect();
            LoadAdditionalSettings();
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            SwitchSettingsColumnState();
        }

        private void HideTable(object sender, RoutedEventArgs e)
        {
            SwitchTablesGridState();
        }
        
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SwitchGraphsGridState();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            ArmLogger.Log("log message");
            ArmLogger.Error("error message");
            ArmLogger.DatabaseLog("DatabaseLog message");
            ArmLogger.ServerLog("ServerLog message");
            ArmLogger.Warning("Warning message");
            ArmLogger.Trace("Trace message");
        }

        private void SetMarkuptoDefault(object sender, RoutedEventArgs e)
        {
            MarkupManager.ToDefaultMarkup();
            ApplyMarkup();
            SaveMarkupValues();
            SaveMarkupToFile();
        }

        private void SplitterDragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            SaveMarkupValues();
            SaveMarkupToFile();
        }

        private void ConnectionControl_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (!DatabaseController.IsConnected)
            {
                InitClientDB();
                ConnectToDatabase();
            }
            else
                DisconnectFromDatabase();
        }

        private bool IsWorking = false;

        private void RdfToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsWorking)
                StartRdf();
            else
                StopRdf();
        }

        private void StartRdf() 
        {
            IsWorking = true;
            _dspClient.SetMode(DspDataModel.Server.DspServerMode.RadioIntelligence);
            _dspClient.EstablishAntennaStateStream();

            var threshold = _dspClient.GetRdfThreshold();
            Dispatcher?.Invoke(() =>
            {
                PanoramaControl.Threshold = threshold - 130;
            });
            Task.Run(WideSpectrumTask);
        }

        private void StopRdf() 
        {
            IsWorking = false;
            _dspClient.SetMode(DspDataModel.Server.DspServerMode.Stop);
            _dspClient.AbortAntennaStateStream();
        }

        private void GetVideoThreshold(bool isMasterThresholds) 
        {
            var wideAttack = _dspClient.GetAttackThreshold(isMasterThresholds, true);
            var widePic = _dspClient.GetPicThreshold(isMasterThresholds, true);
            var narrowAttack = _dspClient.GetAttackThreshold(isMasterThresholds, false);
            var narrowPic = _dspClient.GetPicThreshold(isMasterThresholds, false);

            if (isMasterThresholds)
                VideoGraph.SetMasterThresholds(wideAttack, widePic, narrowAttack, narrowPic);
            else
                VideoGraph.SetSlaveThresholds(wideAttack, widePic, narrowAttack, narrowPic);

            var wideAttackSlider = isMasterThresholds ? MasterWideAttackSlider : SlaveWideAttackSlider;
            var widePicSlider = isMasterThresholds ? MasterWidePicSlider : SlaveWidePicSlider;
            var narrowAttackSlider = isMasterThresholds ? MasterNarrowAttackSlider : SlaveNarrowAttackSlider;
            var narrowPicSlider = isMasterThresholds ? MasterNarrowPicSlider : SlaveNarrowPicSlider;

            wideAttackSlider.Value = wideAttack;
            widePicSlider.Value = widePic;
            narrowAttackSlider.Value = narrowAttack;
            narrowPicSlider.Value = narrowPic;
        }

        private void GetVideoThresholds()
        {
            GetVideoThreshold(true);
            GetVideoThreshold(false);
        }

        private readonly List<SpectrumPanoramaControl.BandwidthParameters> _bandsSettings = 
            new List<SpectrumPanoramaControl.BandwidthParameters>()
            {
                new SpectrumPanoramaControl.BandwidthParameters(3, 1, 3),
                new SpectrumPanoramaControl.BandwidthParameters(30, 1, 29),
                new SpectrumPanoramaControl.BandwidthParameters(25, 1, 24),
                new SpectrumPanoramaControl.BandwidthParameters(30, 3, 29),
                new SpectrumPanoramaControl.BandwidthParameters(15, 1, 15),
                new SpectrumPanoramaControl.BandwidthParameters(30, 1, 29),
                new SpectrumPanoramaControl.BandwidthParameters(23, 1, 21),
                new SpectrumPanoramaControl.BandwidthParameters(30, 6, 29),
                new SpectrumPanoramaControl.BandwidthParameters(24, 1, 22),
                new SpectrumPanoramaControl.BandwidthParameters(30, 12, 29),
                new SpectrumPanoramaControl.BandwidthParameters(20, 1, 2),
                new SpectrumPanoramaControl.BandwidthParameters(100, 1, 95),
                new SpectrumPanoramaControl.BandwidthParameters(500, 34, 466),
            };

        private async Task WideSpectrumTask() 
        {
            while (IsWorking)
            {
                var spectrum3 = _dspClient.GetSpectrum(DspDataModel.FtmRole.Master).Select(p => (double)(p + PanoramaControl.MinSpectrValue)).ToArray();
                var spectrum4 = _dspClient.GetSpectrum(DspDataModel.FtmRole.Slave).Select(p => (double)(p + PanoramaControl.MinSpectrValue)).ToArray();
                Dispatcher?.Invoke(() =>
                {
                    PanoramaControl.spControl.PlotSpectrumF2(SpectrumPanoramaControl.SPControl.GSeries.gSeries,
                        startFreq: 100,
                        dataArray: spectrum3,
                        bandwidthParametersList: _bandsSettings
                        );
                    PanoramaControl.spControl.PlotSpectrumF2(SpectrumPanoramaControl.SPControl.GSeries.gSeries2,
                        startFreq: 100,
                        dataArray: spectrum4,
                        bandwidthParametersList: _bandsSettings
                        );
                });

                if (PanoramaControl.ViewMode == 2)
                {
                    var (spectrum, numberOfPointsPerScan) = _dspClient.GetIntensitySpectrum();
                    var frequencyMhz = AntennaFrequencySetting.GetFrequencyMhzValue();
                    var band = new DspDataModel.Data.Band(Settings.Utilities.GetBandNumber(frequencyMhz));
                    Dispatcher?.Invoke(() =>
                    {
                        PanoramaControl.IQIntensityPaint(spectrum, band.FrequencyFromMhz, band.FrequencyToMhz, numberOfPointsPerScan, 1000);
                        //todo : to config or idk ? I mean number 1000
                    });
                }                

                await Task.Delay(SpectrumDelay);
            }
        }

        private async Task VideoSpectrumTask()
        {
            while (IsVideoSpectrumWorking)
            {
                var ftmRole = (DspDataModel.FtmRole) VideoGraph.Model.Device;
                var videoData = VideoSpectrum(ftmRole);

                Dispatcher?.Invoke(() =>
                {
                    VideoGraph.DrawVideo(videoData);
                });

                await Task.Delay(SpectrumDelay * 5);
            }
        }

        private delegate double[] NarrowDelegate(DspDataModel.FtmRole role);
        private void DrawNarrowGraph(NarrowGraph graph, GraphViewModel model, NarrowDelegate narrowDelegate)
        {
            var masterChecked = model.IsMasterChecked ? 10 : 0;
            var slaveChecked = model.IsSlaveChecked ? 1 : 0;
            switch (masterChecked + slaveChecked)
            {
                case 0:
                    Dispatcher?.Invoke(() =>
                    {
                        graph.ClearChart(0);
                        graph.ClearChart(1);
                    });
                    break;
                case 1:
                    var graphSpectrum = narrowDelegate(DspDataModel.FtmRole.Slave);
                    Dispatcher?.Invoke(() =>
                    {
                        graph.FillChart(1, graphSpectrum);
                        graph.ClearChart(0);
                    });
                    break;
                case 10:
                    var graphSpectrum2 = narrowDelegate(DspDataModel.FtmRole.Master);
                    Dispatcher?.Invoke(() =>
                    {
                        graph.FillChart(0, graphSpectrum2);
                        graph.ClearChart(1);
                    });
                    break;
                case 11:
                    var graphSpectrum3 = narrowDelegate(DspDataModel.FtmRole.Master);
                    var graphSpectrum4 = narrowDelegate(DspDataModel.FtmRole.Slave);
                    Dispatcher?.Invoke(() =>
                    {
                        graph.FillChart(0, graphSpectrum3);
                        graph.FillChart(1, graphSpectrum4);
                    });
                    break;
            }
        }
        private double[] NarrowSpectrum(DspDataModel.FtmRole role) => _dspClient.GetNarrowSpectrum(role).Select(p => (double)p).ToArray();
        private double[] VideoSpectrum(DspDataModel.FtmRole role) => _dspClient.GetVideoSpectrum(role).Select(p => (double)p).ToArray();
        private async Task NarrowSpectrumTask()
        {
            while (IsNarrowBandSpectrumWorking)
            {
                DrawNarrowGraph(NarrowBandGraph, NarrowBandModel, NarrowSpectrum);
                await Task.Delay(SpectrumDelay * 5);
            }
        }

        private void DspConnectionControl_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (!_dspClient.IsConnected)
                _dspClient.Connect();
            else
                _dspClient.Disconnect();
        }

        private void NarrowToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (IsNarrowBandSpectrumWorking)
                return;
            _dspClient.StartNarrowRdfTask();
            IsNarrowBandSpectrumWorking = true;
            _dspClient.EstablishAntennaStateStream();
            Task.Run(NarrowSpectrumTask);
        }

        private void NarrowToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!IsNarrowBandSpectrumWorking)
                return;
            _dspClient.StopNarrowRdfTask();
            IsNarrowBandSpectrumWorking = false;
            _dspClient.AbortAntennaStateStream();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            DatabaseController.Disconnect();
        }

        private void VideoToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (IsVideoSpectrumWorking)
                return;
            _dspClient.StartVideoSpectrumTask();
            IsVideoSpectrumWorking = true;
            _dspClient.EstablishAntennaStateStream();
            Task.Run(VideoSpectrumTask);
        }

        private void VideoToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!IsVideoSpectrumWorking)
                return;
            _dspClient.StopVideoSpectrumTask();
            IsVideoSpectrumWorking = false;
            _dspClient.AbortAntennaStateStream();
        }

        private void StrobeModeToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetStrobeMode(DspDataModel.Hardware.StrobeMode.Auto);
        }

        private void StrobeModeToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetStrobeMode(DspDataModel.Hardware.StrobeMode.Manual);
        }

        private void PairsModeToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetPairsMode(DspDataModel.RdfPairsMode.FastPairs);
        }

        private void PairsModeToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetPairsMode(DspDataModel.RdfPairsMode.FullPairs);
        }

        private void WindowTypeToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetWindowType(DspDataModel.Hardware.WindowFunctionType.RectangleWindow);
        }

        private void WindowTypeToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetWindowType(DspDataModel.Hardware.WindowFunctionType.ChebyshevWindow);
        }

        private void SecondsMarkToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetSecondsMark(DspDataModel.Hardware.SecondsMark.ExternalMark);
        }

        private void SecondsMarkToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetSecondsMark(DspDataModel.Hardware.SecondsMark.InternalMark);
        }

        private void CoefficientToggle_Checked(object sender, RoutedEventArgs e)
        {
            ShowCoefficientColumn();
        }

        private void CoefficientToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            HideCoefficientColumn();
        }

        private void WaterfallToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetWaterfallStrategy(true);
            PanoramaControl.ViewMode = 2;
        }

        private void WaterfallToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            _dspClient.SetWaterfallStrategy(false);
            PanoramaControl.ViewMode = 0;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _dspClient.StartSeriesMeasuringTask();
        }

        private void NoiseCalibrationButton_Click(object sender, RoutedEventArgs e)
        {
            _dspClient.StartNoiseCalibration();
        }

        private void IqGatheringButton_Click(object sender, RoutedEventArgs e)
        {
            _dspClient.StartIqGathering();
        }
    }
}
