﻿using System.Windows;
using CuirassArmDataModel.Markup;

namespace CuirassArm
{
    public partial class MainWindow
    {
        private void SaveMarkupValues() 
        {
            SaveTablesSplitterPosition();
            SaveGraphsSplitterPosition();
            SaveTablesGrid();
            SaveGraphsGrid();
        }

        private void SaveMarkupToFile() => MarkupManager.Save();

        #region Settings
        private void SwitchSettingsColumnState()
        {
            if (Markup.AreSettingsVisible)
                HideSettingsColumn();
            else
                ShowSettingsColumn();

            SaveMarkupValues();
            SaveMarkupToFile();
        }

        private void ShowSettingsColumn()
        {
            if (_settingsColumnState == CuirassArm.Markup.SettingsColumnState.CoefficientVisible)
            {
                return;
            }
            MainGrid.ColumnDefinitions[1].Width = new GridLength(MarkupConstants.SettingsDefaultSize);
            PropertiesControl.Visibility = Visibility.Visible;
            Markup.AreSettingsVisible = true;
            _settingsColumnState = CuirassArm.Markup.SettingsColumnState.LocalGlobalVisible;
        }
        private void HideSettingsColumn()
        {
            if (_settingsColumnState == CuirassArm.Markup.SettingsColumnState.CoefficientVisible)
                return;
            MainGrid.ColumnDefinitions[1].Width = new GridLength(0);
            PropertiesControl.Visibility = Visibility.Collapsed;
            Markup.AreSettingsVisible = false;
            _settingsColumnState = CuirassArm.Markup.SettingsColumnState.Hidden;
        }

        private void ShowCoefficientColumn()
        {
            if (_settingsColumnState == CuirassArm.Markup.SettingsColumnState.LocalGlobalVisible)
            {
                PropertiesControl.Visibility = Visibility.Collapsed;
                Markup.AreSettingsVisible = false;
            }
            else 
            {
                ShowSettingsColumn();
                PropertiesControl.Visibility = Visibility.Collapsed;
                Markup.AreSettingsVisible = false;
            }
            CoefficientsTab.Visibility = Visibility.Visible;
            _settingsColumnState = CuirassArm.Markup.SettingsColumnState.CoefficientVisible;
        }
        private void HideCoefficientColumn()
        {
            if (_settingsColumnState == CuirassArm.Markup.SettingsColumnState.LocalGlobalVisible)
                return;
            _settingsColumnState = CuirassArm.Markup.SettingsColumnState.Hidden;
            CoefficientsTab.Visibility = Visibility.Collapsed;
            HideSettingsColumn();
        }
        #endregion
        #region TablesGrid
        private void SwitchTablesGridState()
        {
            if (!Markup.AreGraphsVisible)
                return;

            if (Markup.AreTablesVisible)
            {                
                SaveMarkupValues();
                HideTablesGrid(); 
                SaveMarkupToFile();
            }
            else
            {
                ShowTablesGrid();
                SaveMarkupValues();
                SaveMarkupToFile();
            }
        }

        private void ShowTablesGrid()
        {
            if (Markup.TablesGridLength == 0)
                return;
            WorkspaceGrid.RowDefinitions[2].MinHeight = MarkupConstants.SecondaryTableMinWidth;
            WorkspaceGrid.RowDefinitions[2].Height = new GridLength(Markup.TablesGridLength, GridUnitType.Star);
            TablesGrid.Visibility = Visibility.Visible;
            ShowGraphsTablesSplitter();
            Markup.AreTablesVisible = true;
        }
        private void HideTablesGrid()
        {
            WorkspaceGrid.RowDefinitions[2].MinHeight = 0;
            WorkspaceGrid.RowDefinitions[2].Height = new GridLength(0);
            TablesGrid.Visibility = Visibility.Collapsed;
            HideGraphsTablesSplitter();
            Markup.AreTablesVisible = false;
        }

        private void SaveTablesGrid()
        {
            if(Markup.AreTablesVisible)
                Markup.TablesGridLength = WorkspaceGrid.RowDefinitions[2].Height.Value;
        }
        #endregion
        #region GraphsGrid
        private void SwitchGraphsGridState()
        {
            if (!Markup.AreTablesVisible)
                return;

            if (Markup.AreGraphsVisible)
            {
                SaveMarkupValues();
                HideGraphsGrid();
                SaveMarkupToFile();
            }
            else
            {
                ShowGraphsGrid();
                SaveMarkupValues();
                SaveMarkupToFile();
            }
        }

        private void ShowGraphsGrid()
        {
            if (Markup.GraphsGridLength == 0)
                return;
            WorkspaceGrid.RowDefinitions[0].MinHeight = MarkupConstants.SecondaryTableMinWidth;
            WorkspaceGrid.RowDefinitions[0].Height = new GridLength(Markup.GraphsGridLength, GridUnitType.Star);
            GraphsGrid.Visibility = Visibility.Visible;
            ShowGraphsTablesSplitter();
            Markup.AreGraphsVisible = true;
        }
        private void HideGraphsGrid()
        {
            WorkspaceGrid.RowDefinitions[0].MinHeight = 0;
            WorkspaceGrid.RowDefinitions[0].Height = new GridLength(0);
            GraphsGrid.Visibility = Visibility.Collapsed;
            HideGraphsTablesSplitter();
            Markup.AreGraphsVisible = false;
        }

        private void SaveGraphsGrid()
        {
            if (Markup.AreGraphsVisible)
                Markup.GraphsGridLength = WorkspaceGrid.RowDefinitions[0].Height.Value;
        }
        #endregion
        #region Splitters
        private void SaveTablesSplitterPosition()
        {
            Markup.TablesGridLeftSplitterPosition = TablesGrid.ColumnDefinitions[0].Width.Value;
            Markup.TablesGridRightSplitterPosition = TablesGrid.ColumnDefinitions[2].Width.Value;
        }

        private void ApplyTablesSplitterPosition()
        {
            if (Markup.TablesGridLeftSplitterPosition == 0)
                return;
            TablesGrid.ColumnDefinitions[0].MinWidth = MarkupConstants.MainTableMinWidth;
            TablesGrid.ColumnDefinitions[2].MinWidth = MarkupConstants.SecondaryTableMinWidth;
            TablesGrid.ColumnDefinitions[0].Width = new GridLength(Markup.TablesGridLeftSplitterPosition, GridUnitType.Star);
            TablesGrid.ColumnDefinitions[2].Width = new GridLength(Markup.TablesGridRightSplitterPosition, GridUnitType.Star);
        }

        private void SaveGraphsSplitterPosition()
        {
            Markup.GraphsGridLeftSplitterPosition = GraphsGrid.ColumnDefinitions[0].Width.Value;
            Markup.GraphsGridRightSplitterPosition = GraphsGrid.ColumnDefinitions[2].Width.Value;
        }

        private void ApplyGraphsSplitterPosition()
        {
            if (Markup.TablesGridLeftSplitterPosition == 0)
                return;
            GraphsGrid.ColumnDefinitions[0].MinWidth = MarkupConstants.MainTableMinWidth;
            GraphsGrid.ColumnDefinitions[2].MinWidth = MarkupConstants.SecondaryTableMinWidth;
            GraphsGrid.ColumnDefinitions[0].Width = new GridLength(Markup.GraphsGridLeftSplitterPosition, GridUnitType.Star);
            GraphsGrid.ColumnDefinitions[2].Width = new GridLength(Markup.GraphsGridRightSplitterPosition, GridUnitType.Star);
        }

        private void ShowGraphsTablesSplitter()
        {
            WorkspaceGrid.RowDefinitions[1].MinHeight = MarkupConstants.SplitterSize;
            WorkspaceGrid.RowDefinitions[1].Height = new GridLength(MarkupConstants.SplitterSize);
            WorkspaceSplitter.Visibility = Visibility.Visible;
        }

        private void HideGraphsTablesSplitter()
        {
            WorkspaceGrid.RowDefinitions[1].MinHeight = 0;
            WorkspaceGrid.RowDefinitions[1].Height = new GridLength(0);
            WorkspaceSplitter.Visibility = Visibility.Collapsed;
        }
        #endregion
               
        private void SwapGraphsTableContents() 
        {
            var one = new GridLength(GraphsGrid.ColumnDefinitions[0].Width.Value, GraphsGrid.ColumnDefinitions[0].Width.GridUnitType);
            var two = new GridLength(GraphsGrid.ColumnDefinitions[2].Width.Value, GraphsGrid.ColumnDefinitions[2].Width.GridUnitType);
            GraphsGrid.ColumnDefinitions[2].Width = one;
            GraphsGrid.ColumnDefinitions[0].Width = two;
            //we do not save this state after swap
        }

        private void ApplyMarkup() 
        {
            if (Markup.AreSettingsVisible)
                ShowSettingsColumn();
            else
                HideSettingsColumn();

            if (Markup.AreTablesVisible)
                ShowTablesGrid();
            else
                HideTablesGrid();

            if (Markup.AreGraphsVisible)
                ShowGraphsGrid();
            else
                HideGraphsGrid();

            if (!Markup.AreGraphsVisible || !Markup.AreTablesVisible)
                HideGraphsTablesSplitter();

            ApplyTablesSplitterPosition();
            ApplyGraphsSplitterPosition();
        }
    }
}
