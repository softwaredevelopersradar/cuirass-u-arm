﻿namespace CuirassArm.Markup
{
    public enum SettingsColumnState 
    {
        Hidden = 0,
        LocalGlobalVisible = 1,
        CoefficientVisible = 2
    }
}
