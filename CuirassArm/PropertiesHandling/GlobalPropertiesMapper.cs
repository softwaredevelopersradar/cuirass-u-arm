﻿using AutoMapper;

namespace CuirassArm.PropertiesHandling
{
    public static class GlobalPropertiesMapper
    {
        private static readonly Mapper _mapper;

        static GlobalPropertiesMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProfileCtrltoDB>();
                cfg.AddProfile<ProfileDBToCtrl>();
                cfg.CreateMap<DllCuirasaUProperties.Models.GlobalProperties, KirasaUModelsDBLib.GlobalProperties>()
                .ReverseMap();
            });
            _mapper = new Mapper(config);
        }

        public static DllCuirasaUProperties.Models.GlobalProperties Map(KirasaUModelsDBLib.GlobalProperties source)
        {
            return _mapper.Map<KirasaUModelsDBLib.GlobalProperties, DllCuirasaUProperties.Models.GlobalProperties>(source);
        }
        public static KirasaUModelsDBLib.GlobalProperties Map(DllCuirasaUProperties.Models.GlobalProperties source)
        {
            return _mapper.Map<DllCuirasaUProperties.Models.GlobalProperties, KirasaUModelsDBLib.GlobalProperties>(source);
        }
    }
}

