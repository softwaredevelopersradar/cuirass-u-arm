﻿using System;
using DllCuirasaUProperties;
using CuirassArmDataModel.Database;

namespace CuirassArm.PropertiesHandling
{
    public static class PropertiesEventHandler
    {
        private static IDatabaseController _databaseController;
        private static ControlProperties _propertiesControl;

        public static void InitializePropertiesControl(ControlProperties propertiesControl) 
        {
            _propertiesControl = propertiesControl;

            LoadLocalProperties();

            _propertiesControl.OnLocalPropertiesChanged += LocalPropertiesChanged;
            _propertiesControl.OnLocalDefaultButtonClick += SetLocalPropertiesToDefault;

            _propertiesControl.OnGlobalPropertiesChanged += GlobalPropertiesInControlChanged;
            _propertiesControl.OnGlobalDefaultButtonClick += SetGlobalPropertiesToDefault;
        }

        private static void LoadLocalProperties()
        {
            try
            {
                var localProperties = LocalPropertiesSaver.Load();
                _propertiesControl.Local = localProperties;
            }
            catch (Exception e)
            {
                //when local properties file does not exist
                //exception ignored, properties are filled with default values
                SetLocalPropertiesToDefault(e, EventArgs.Empty);
            }
        }

        public static void InitializeDatabaseController(IDatabaseController databaseController)
        {
            _databaseController = databaseController;
            _databaseController.OnGlobalPropertiesUpdated += GlobalPropertiesInDatabaseChanged;
        }

        private static void GlobalPropertiesInDatabaseChanged(object sender, KirasaUModelsDBLib.GlobalProperties globalProperties)
        {
            _propertiesControl.Global = GlobalPropertiesMapper.Map(globalProperties);
        }

        private static void SetLocalPropertiesToDefault(object sender, EventArgs e)
        {
            _propertiesControl.Local.DB.IpAddress = DefaultProperties.DatabaseAddress;
            _propertiesControl.Local.DB.Port = DefaultProperties.DatabasePort;

            _propertiesControl.Local.DF.IpAddress = DefaultProperties.DspAddress;
            _propertiesControl.Local.DF.Port = DefaultProperties.DspPort;
        }

        private static void LocalPropertiesChanged(object sender, DllCuirasaUProperties.Models.LocalProperties localProperties)
        {
            LocalPropertiesSaver.Save(_propertiesControl.Local);
        }

        private static void SetGlobalPropertiesToDefault(object sender, EventArgs e)
        {
            //todo 
        }

        private static void GlobalPropertiesInControlChanged(object sender, DllCuirasaUProperties.Models.GlobalProperties globalProperties)
        {
            //todo 
        }
    }
}
