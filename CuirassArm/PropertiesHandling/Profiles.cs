﻿using AutoMapper;

namespace CuirassArm.PropertiesHandling
{
    public class ProfileCtrltoDB : Profile
    {
        public ProfileCtrltoDB()
        {
            CreateMap<DllCuirasaUProperties.Models.Global.Calibration, KirasaUModelsDBLib.Calibration>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<DllCuirasaUProperties.Models.Global.Location, KirasaUModelsDBLib.Coord>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<DllCuirasaUProperties.Models.Global.RadioIntelegence, KirasaUModelsDBLib.RadioIntelegence>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
        }
    }

    public class ProfileDBToCtrl : Profile
    {
        public ProfileDBToCtrl()
        {
            CreateMap<KirasaUModelsDBLib.Calibration, DllCuirasaUProperties.Models.Global.Calibration>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<KirasaUModelsDBLib.Coord, DllCuirasaUProperties.Models.Global.Location>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<KirasaUModelsDBLib.RadioIntelegence, DllCuirasaUProperties.Models.Global.RadioIntelegence>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
        }
    }
}
