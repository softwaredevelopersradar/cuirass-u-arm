﻿using System;
using System.IO;
using System.Text;
using DllCuirasaUProperties.Models;
using DspDataModel;
using YamlDotNet.Serialization;

namespace CuirassArm.PropertiesHandling
{
    public static class LocalPropertiesSaver
    {
        private const string LocalPropertiesFileName = "LocalProperties.yaml";
        private readonly static object _lockObject = new object();

        public static void Save(LocalProperties properties)
        {
            try
            {
                lock (_lockObject)
                {
                    var serializer = new SerializerBuilder().Build();
                    using (var fs = File.Open(LocalPropertiesFileName, FileMode.Create))
                    using (var sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        serializer.Serialize(sw, properties);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex, "Can't save config");
            }
        }

        public static LocalProperties Load()
        {
            lock (_lockObject)
            {
                var deserializer = new DeserializerBuilder().Build();
                return deserializer.Deserialize<LocalProperties>(File.ReadAllText(LocalPropertiesFileName));
            }
        }
    }
}
