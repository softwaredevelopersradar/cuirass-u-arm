﻿namespace CuirassArm.PropertiesHandling
{
    public static class DefaultProperties
    {
        public const string DatabaseAddress = "127.0.0.1";
        public const int DatabasePort = 8302;

        public const string DspAddress = "127.0.0.1";
        public const int DspPort = 12345;
    }
}
